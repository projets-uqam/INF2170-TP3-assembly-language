SORT
===================================================
TP3 - E2016 - INF2170 


Summary
---------
* This program sort and display in ascending order the values input from user


Technologies used:
------------------
* PEP/8 Machine Language Programming


PEP/8
----------
* Pep/8 is a virtual machine for writing machine language and assembly
language programs. 
* Programmed by: Chris Dimpfl and J. Stanley Warford


Sorting Rules:
--------------
1. Number of characters
2. Alphabetical ordering
3. Numerical order 


Input values Rules
------------------
* Be composed of the dash symbol '-' and be separated by spaces
* Have 1 to 4 characters (a-zA-Z) or (0-9)
* The first character must be a capital letter, example: -A2
* Not more than 5 characters while including the dash symbol '-'


Execution example
-----------------
```sh
Welcome to the program "SORT" !

Please enter the values to be processed («.» - to finish):
-X23a -Z23b -X2 -Xa2 -XA

order: -> -XA -> -X2 -> -Xa2 -> -X23a -> -Z23b 

Please enter the values to be processed («.» - to finish):
 -A12 -B -A11b -A11 -A11a

order: -> -B -> -A11 -> -A12 -> -A11a -> -A11b 

Please enter the values to be processed («.» - to finish):
-Z -Z -Z1 -Z11z -Z1 -Z11

order: -> -Z -> -Z -> -Z1 -> -Z1 -> -Z11 -> -Z11z 

Please enter the values to be processed («.» - to finish):
-F   -A23r -A25r -A1 -A1a -Aa

order: -> -F -> -A1 -> -Aa -> -A1a -> -A23r -> -A25r 

Please enter the values to be processed («.» - to finish):
 -Q8 -Q7 -Q6 -Q5 -Q4 -Q3 -Q2 -Q1

order: -> -Q1 -> -Q2 -> -Q3 -> -Q4 -> -Q5 -> -Q6 -> -Q7 -> -Q8 

Please enter the values to be processed («.» - to finish):
.

End Program.
```


License
--------
This project is licensed under the Apache License - see [here](https://www.gnu.org/licenses/gpl-3.0.en.html) for details
