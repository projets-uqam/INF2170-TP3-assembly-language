;
; /-----------Main-Program----------/
;
          CALL    bienvenu   ; message d'entête
début:    CALL    question   ; message de la question à l'usager 
          CALL    lecture    ; lecture de la série des données entrées         
          BR      onTermin   ; on finit l'exécution du programme ?
onCont1:  BR      onRelanc   ; on relance le programme ?         
onCont2:  CALL    triage     ; triage de la série des données entrées
          CALL    affResul   ; affichage du résultat                    
          BR      début      ; relancement du processus
fin:      STRO    normale,d  ; message de fin normal du programme 
          STOP

;/----------- Sub-Programmes----------/
;     
; affichage du paragraphe de bienvenue, le mode d'emploi et le règles
;
bienvenu:STRO    paragrap,d 
         RET0
;     
; affichage de la question à l'usager
;
question:STRO    demande,d 
         RET0
;
; lecture d'une chaîne de caractères   
;
lecture: CALL    reInit0     ; vider le tableau «la chaîne de caractères» 
         CALL    reInit1     ; réinitialisation des variables
         CHARO   '\n',i      ; affiche le symbole de saut de ligne
boucleLE:CHARI   tab,x       ; lecture d'un caractère 
         LDA     0,i         ; efface le registre A
         LDBYTEA tab,x       ; charge le caractère lu
         ADDX    1,i         ; position suivante        
         CPA     '.',i       ; est-ce le POINT de fin ?
         BREQ    errOuTer    ; erreur d'entrée des données ou décision de terminer ?
         BRNE    contLE      ; continuer avec la lecture
         BR      finlect
contLE:  CPA     10,i        ; dernier caractère ?
         BREQ    derval      ; dernier valeur et oui, c'est terminé         
         CALL    valcarac    ; validation d'un caractère
         LDBYTEA err,d       ; après la validation, on termine ?
         CPA     1,i
         BREQ    finlect     ; oui, on termine
         BR      boucleLE 
finlect: RET0            
;
; determiner si c'est un erreur d'entrée des données ou une décision de terminer ?
;
errOuTer:LDA     0,i         ; efface le registre A
         LDA     cptCar,d    ; charge le compteur de caractères lus                      
         BRNE    estErrED    ; c'est une erreur d'entrée des données
         CALL    termine     ; l'utilisateur veut terminer le programme
         BR      finlect     ; branchement à fin de la lecture
estErrED:CALL    relancer    ; affichage du message d'erreur et mettre à jours la sentinelle d'erreur     
         BR      finlect     ; branchement à fin de la lecture
;
; vider le tableau «la chaîne de caractères» 
;
reInit0: LDA     0,i         ; efface le registre A
         STA     tab,x        
         ADDX    2,i         ; suivant    
         CPX     40,i        ; maximum de caractères 
         BREQ    finReIni    ; fin de la boucle     
         BRLT    reInit0    
finReIni:LDX     0,i
         RET0             
;
; réinitialisation des variables
;
reInit1: LDX     0,i         ; début de la chaîne
         LDA     0,i         ; efface le registre A
         STA     cptCar,d    ; réinitialise le compteur de caractères lus
         STA     cptVal,d    ; réinitialise le compteur de valeurs lus
         STA     posTabIL,d  ; réinitialise la position dans le tableau «tabInLon»
         LDBYTEA 0,i
         STBYTEA terminer,d  ; réinitialise la sentinelle de sortie du programme
         STBYTEA tiret,d     ; réinitialise la sentinelle de tiret
         STBYTEA err,d       ; réinitialise la sentinelle de erreur
         STBYTEA derVal,d    ; réinitialise la sentinelle de la dernière valeur  
         RET0
;
; réinitialisation des variables
;
reInit2: LDA     0,i         ; efface le registre A
         STA     cptChiff,d  ; réinitialise le compteur de chiffres lus dans chaque valeur
         STA     cptLMaj,d   ; réinitialise le compteur de lettres majuscules lus dans chaque valeur
         STA     cptLMin,d   ; réinitialise le compteur de lettres minuscules lus dans chaque valeur
         STA     cptCaVa,d   ; réinitialise le compteur de caractères lus dans chaque valeur
         STA     cptTiret,d  ; réinitialise le compteur de tirets lus dans chaque valeur
         STA     cptLett,d   ; réinitialise le compteur de lettres lus dans chaque valeur
         RET0
;
; après la lecture....
; on finis l'exécution du programme ?

onTermin:LDBYTEA terminer,d ; après la lecture, on termine ?
         CPA     1,i
         BREQ    fin        ; oui, on termine
         BRNE    onCont1    ; on continue
;
; après la lecture....
; on relance le programme ?

onRelanc:LDBYTEA err,d      ; après la lecture, on relance ?
         CPA     1,i
         BREQ    début      ; oui, on relance
         BRNE    onCont2    ; on continue        
; 
; se débarrasser du « ENTREE » et sortie du programme
;
termine: LDA     0,i
         CHARI   caract,d    ; caractère pour se débarrasser du «ENTREE» 
         LDBYTEA caract,d    ; caractère lus
         CPA     10,i        ; caractère «enter» ?
         BRNE    termine     ; relire termine jusqu'au ENTREE
         CALL    sentterm    ; sentinelle de sortie du programme 
         RET0
;
; déterminer si on termine le programme
;
sentterm:LDBYTEA terminer,d
         ADDA    1,i
         STBYTEA terminer,d  ; si terminer = 1 -> on termine
         RET0 
;
; validation d'un caractère
;
valcarac:CPA     '0',i       ; < 0 ?
         BRLT    paschiff    ; non, ce n'est pas un chiffre 
         CPA     '9',i       ; > 9 ?
         BRGT    paschiff    ; non, ce n'est pas un chiffre
         BRLE    compt3      ; oui, c'est un chiffre         
paschiff:CPA     'A',i       ; < A ?
         BRLT    pasmaj      ; non, ce n'est pas une lettre majuscule 
         CPA     'Z',i       ; > Z ?
         BRGT    pasmaj      ; non, ce n'est pas une lettre majuscule
         BRLE    compt4      ; oui, c'est une lettre majuscule      
pasmaj:  CPA     'a',i       ; < a ?
         BRLT    pasmin      ; non, ce n'est pas une lettre minuscule
         CPA     'z',i       ; > z ?
         BRGT    pasmin      ; non, ce n'est pas une lettre minuscule
         BRLE    compt5      ; oui, c'est une lettre minuscule         
pasmin:  CPA     '-',i       ; déterminer si le caractère est un tiret
         BREQ    compt8      ; compteur de tirets lus    
         CPA     ' ',i       ; déterminer si le caractère est un espace
         BREQ    compt2      ; compteur de valeur lus          
pastiret:CALL    relancer    ; relancement du processus avec erreur 
finvalca:RET0                ; fin de la validation d'un caractère

;
; validation d'une valeur
;
valVal: LDA      0,i         ; efface le registre A
        LDA      cptCaVa,d   ; charge le compteur de caractères lus dans chaque valeur
        CPA      5,i         ; chaque valeur ne doit pas dépasser 5 caractères incluant le tiret
        BRLE     contVV1     ; on continue la validation de la valeur
        CALL     relancer    ; affichage du message d'erreur et mettre à jours le sentinelle d'erreur
        BR       contVV4
contVV1:LDA      0,i         ; efface le registre A
        LDA      cptLMaj,d   ; charge le compteur de lettres majuscules lus dans chaque valeur
        CPA      0,i         ; le premier caractère est obligatoirement une lettre majuscule
        BRGT     contVV2     ; on continue la validation de la valeur
        CALL     relancer    ; affichage du message d'erreur et mettre à jours le sentinelle d'erreur
        BR       contVV4
contVV2:LDA      0,i         ; efface le registre A    
        LDA      cptTiret,d  ; charge le compteur de tirets lus dans chaque valeur
        CPA      1,i         ; chaque valeur est composée du symbole tiret(-)
        BREQ     contVV3     ; on continue la validation de la valeur
        CALL     relancer    ; affichage du message d'erreur et mettre à jours le sentinelle d'erreur
        BR       contVV4
contVV3:LDA      0,i         ; efface le registre A
        LDA      cptLett,d   ; charge le compteur de lettres lus dans chaque valeur
        CPA      2,i         ; chaque valeur ne peut contenir que 2 lettres
        BRLE     contVV4     ; on continue la validation de la valeur
        CALL     relancer    ; affichage du message d'erreur et mettre à jours le sentinelle d'erreur
        BR       contVV4
contVV4:RET0
;
; vérifier la limite du maximum des caractères
;
verfMaCa:LDA     cptCar,d    ; compteur de caractères lus
         CPA     40,i        ; maximum 40 caractères (sans l'Entrée)
         BRLE    contVerf
         CALL    relancer    ; affichage du message d'erreur et mettre à jours la sentinelle d'erreur
contVerf:RET0
;  
; le compteur de caractères lus + 1
;
compt1:  LDA     cptCar,d    ; compteur de caractères lus
         ADDA    1,i         ; compteur de caractères lus + 1
         STA     cptCar,d    ; sauvegarde le compteur de caractères lus
         CALL    verfMaCa    ; vérifier la limite du maximum des caractères
         RET0
;  
; le compteur de valeurs et on continue la validation de la chaîne
;
compt2:  LDA     cptCar,d    ; le caractère espace entrée au début de la chaîne 
         BREQ    finCpt2
         LDA     cptCaVa,d   ; le caractère espace entrée après un autre espace
         BREQ    finCpt2
         LDA     cptVal,d    ; compteur de valeurs lus
         ADDA    1,i         ; compteur de valeurs lus + 1
         STA     cptVal,d    ; sauvegarde le compteur de valeurs lus
         CALL    valVal      ; validation d'une valeur
         CALL    stockLon    ; sauvegarder la longueur de chaque valeur       
         CALL    reInit2     ; réinitialisation des variables       
finCpt2: BR      finvalca    ; fin de la validation d'un caractère 
;  
; le compteur de valeurs «dernière valeur» et on termine la lecture de la chaîne
;
derval:  CALL    sentDeVa    ; mettre à jours la sentinelle de la dernière valeur
         LDA     cptVal,d    ; compteur de valeurs lus 
         ADDA    1,i         ; compteur de valeurs lus + 1
         STA     cptVal,d    ; sauvegarde le compteur de valeurs lus
         CALL    valVal      ; validation d'une valeur
         CALL    stockLon    ; sauvegarder la longueur de chaque valeur              
         CALL    reInit2     ; réinitialisation des variables                     
         BR      finlect     ; fin de la lecture de la chaîne
;  
; le compteur de chiffres lus dans chaque valeur + 1
;
compt3:  LDA     cptChiff,d  ; compteur de chiffres lus dans chaque valeur
         ADDA    1,i         ; compteur de chiffres lus dans chaque valeur + 1
         STA     cptChiff,d  ; sauvegarde le compteur de chiffres lus
         CALL    compt1      ; compteur de caractères lus 
         CALL    compt6      ; compteur de caractères lus dans chaque valeur 
         BR      finvalca    ; fin de la validation d'un caractère 
;  
; le compteur de lettres majuscules lus dans chaque valeur + 1
;
compt4:  LDA     cptLMaj,d   ; compteur de lettres majuscules lus dans chaque valeur
         ADDA    1,i         ; compteur de lettres majuscules lus dans chaque valeur + 1
         STA     cptLMaj,d   ; sauvegarde le compteur de lettres majuscules lus
         CALL    compt1      ; compteur de caractères lus
         CALL    compt6      ; compteur de caractères lus dans chaque valeur
         CALL    compt7      ; compteur de lettre lus 
         BR      finvalca    ; fin de la validation d'un caractère 
;  
; le compteur de lettres minuscules lus dans chaque valeur + 1
;
compt5:  LDA     cptLMin,d   ; compteur de lettres minuscules lus dans chaque valeur
         ADDA    1,i         ; compteur de lettres minuscules lus dans chaque valeur + 1
         STA     cptLMin,d   ; sauvegarde le compteur de lettres minuscules lus
         CALL    compt1      ; compteur de caractères lus
         CALL    compt6      ; compteur de caractères lus dans chaque valeur
         CALL    compt7      ; compteur de lettre lus 
         BR      finvalca    ; fin de la validation d'un caractère 
;  
; compteur de caractères lus dans chaque valeur + 1
;
compt6:  LDA     cptCaVa,d   ; compteur de caractères lus dans chaque valeur
         ADDA    1,i         ; compteur de caractères lus dans chaque valeur + 1
         STA     cptCaVa,d   ; sauvegarde le compteur de caractères lus dans chaque valeur
         RET0
;  
; compteur de lettres lus dans chaque valeur + 1
;
compt7:  LDA     cptLett,d   ; compteur de lettres lus dans chaque valeur
         ADDA    1,i         ; compteur de lettres lus dans chaque valeur + 1
         STA     cptLett,d   ; sauvegarde le compteur de lettres lus dans chaque valeur
         RET0
;  
; compteur de tirets lus dans chaque valeur + 1
;
compt8:  LDA     cptTiret,d  ; compteur de tirets lus dans chaque valeur
         ADDA    1,i         ; compteur de tirets lus dans chaque valeur + 1
         STA     cptTiret,d  ; sauvegarde le compteur de tirets lus
         CALL    compt1      ; compteur de caractères lus
         CALL    compt6      ; compteur de caractères lus dans chaque valeur
         CALL    stockInd    ; sauvegarder l'indice du début de chaque valeur  
         BR      finvalca    ; fin de la validation d'un caractère 
;
; déterminer si la valeur a un tiret et mettre à jours le compteur de caractères lus
;
senttire:LDBYTEA tiret,d
         ADDA    1,i
         STBYTEA tiret,d     ; si tiret = 1 -> la valeur a un tiret
         CALL    compt1      ; compteur de caractères lus
         CALL    compt6      ; compteur de caractères lus dans chaque valeur
         BR      finvalca    ; fin de la validation d'un caractère     
; 
; se débarrasser du ENTREE avec message d'erreur
;
relancer:LDBYTEA derVal,d    ; est-ce que c'est la dernière valeur ?
         CPA     1,i
         BREQ    contRela
         CHARI   caract,x    ; continuer la lecture du tampon
         LDBYTEA caract,x 
         CPA     10,i        ; caractère «enter» ?
         BRNE    relancer    ; relire relancer jusqu'au «ENTREE»
contRela:CALL    erreur
         RET0
;
; affichage du message d'erreur et mettre à jours le sentinelle d'erreur
;
erreur:  STRO    msgerreu,d
         CALL    senterr 
         RET0
;
; déterminer s'il y a une erreur dans l'entrée des données
;
senterr: LDBYTEA err,d
         ADDA    1,i
         STBYTEA err,d       ; si err = 1 -> il y a une erreur
         RET0
;
; déterminer si c'est la dernière valeur
;
sentDeVa:LDBYTEA derVal,d
         ADDA    1,i
         STBYTEA derVal,d       ; si derVal = 1 -> c'est la dernière valeur
         RET0
;
; déterminer si le caractère est un chiffre
;
sentChif:STBYTEA carCompT,d 
         CPA     '0',i       ; < 0 ?
         BRLT    nonChiff    ; non, ce n'est pas un chiffre 
         CPA     '9',i       ; > 9 ?
         BRGT    nonChiff    ; non, ce n'est pas un chiffre   
         LDBYTEA chiffre,d 
         ADDA    1,i
         STBYTEA chiffre,d   ; si chiffre = 1 -> c'est un chiffre
         LDBYTEA carCompT,d
         STBYTEA iCar,d
nonChiff:RET0
;
; déterminer si le caractère est une lettre
;
sentLett:STBYTEX carCompT,d
         CPX     'A',i       ; < A ?
         BRLT    pasmaj1     ; non, ce n'est pas une lettre majuscule 
         CPX     'Z',i       ; > Z ?
         BRGT    pasmaj1     ; non, ce n'est pas une lettre majuscule
         BR      estMaj    
pasmaj1: CPX     'a',i       ; < a ?
         BRLT    noLett      ; non, ce n'est pas une lettre minuscule
         CPX     'z',i       ; > z ?
         BRGT    noLett      ; non, ce n'est pas une lettre minuscule
estMaj:  LDBYTEX lettre,d 
         ADDX    1,i
         STBYTEX lettre,d    ; si lettre = 1 -> c'est une lettre
         LDBYTEX carCompT,d
         STBYTEX jCar,d
noLett:  RET0
;
; determiner s'il y a supériorité des caractères
; les chiffres sont supérieurs aux lettres
;
valSupCa:STBYTEA carCompT,d  ; sauvegarder le caractère de comparaison
         LDBYTEA chiffre,d
         BRNE    contVaS1
         BREQ    finValSu    ; pas de supériorité
contVaS1:LDBYTEA lettre,d 
         BRNE    decision    ; il faut inverser l'ordre
         BREQ    finValSu    ; se terminer la validation de supériorité des caractères
decision:LDBYTEA carCompT,d
         STBYTEA iCar,d      ; restaurer le caractère de comparaison
         BR      inverLo1    ; les chiffres sont supérieurs aux lettres
finValSu:LDBYTEA carCompT,d
         STBYTEA iCar,d      ; restaurer le caractère de comparaison
         BR      contCom1    
;
; sauvegarder l'indice du début de chaque valeur dans un tableau: tabInLon
; ********************************************************************  
; tabInLon : tabInLon [0] = l'indice de la première valeur
;  
stockInd:STX     posTab,d    ; déterminer le tab[x] du début de chaque valeur
         LDA     posTab,d
         SUBA    1,i     
         LDX     posTabIL,d  ; position du tableau des indices et des longueurs 
         STA     tabInLon,x  ; sauvegarder l'indice dans tabInLon[x]
         ADDX    2,i         ; position suivant
         STX     posTabIL,d  ; sauvegarder la position
         LDX     posTab,d    ; restaure la position dans tab
         RET0
;
; sauvegarder la longueur de chaque valeur dans un tableau: tabInLon
; ********************************************************************  
; tabInLon : tabInLon [1] = la longueur de la première valeur
;
stockLon:STX     posTab,d    ; conserve la valeur du registre X
         LDA     cptCaVa,d   ; le compteur de caratères lus dans chaque valeur
         LDX     posTabIL,d  ; position du tableau des indices et des longueurs
         STA     tabInLon,x  ; sauvegarder la longueur dans tabInLon[x]
         ADDX    2,i         ; position suivant
         STX     posTabIL,d  ; sauvegarder la position
         LDX     posTab,d    ; restaure la position dans «tab»
         RET0
;
; le triage en fonction de la longueur de la valeur puis en ordre des caractères
;  
triage: CALL     tri
        RET0
;
; ordonner le tableau «tabInLon» par longueur 
;
tri:     LDA     cptVal,d    ; la quantité de valeurs à trier
         SUBA    1,i
         STA     cptBOrLo,d
bouclOr0:LDA     cptBOrLo,d  ; combien de fois repeter le triage 
         CPA     0,i
         BREQ    finTriLo    ; fin du triage         
         CALL    ordLong
         LDA     cptBOrLo,d
         SUBA    1,i
         STA     cptBOrLo,d
         CALL    reInit3     ; réinitialisation des variables 
         BR      bouclOr0
finTriLo:RET0
;
; ordonner deux valeurs par longueur «placées consécutivement dans le tableau: tab»    
; 
ordLong: LDX     0,i
         LDA     cptVal,d
         SUBA    1,i 
         STA     nbIterac,d  ; nombre d'itérations pour ordonner deux valeurs dans «tabInLon»
     
bouclOr1:LDA     nbIterac,d
         CPA     0,i
         BREQ    finOrLon    ; on termine d'ordonner par longueur
         SUBA    1,i
         STA     nbIterac,d  ; mettre à jours le nombre d'itérations
         LDX     xGeneral,d
         CPX     0,i
         BREQ    contOr1     ; continuer à ordonner le tableau «tabInLon»
         BRNE    reculer1    ; il faut reculer, car ce n'est pas la première comparaison
reculer1:SUBX    2,i         ; l'antérieure longueur a été déjà comparée 
;
; sauvegarder les variables
; l'indice et la longueur: valeur #1 
; l'indice et la longueur: valeur #2
;
contOr1: LDA     tabInLon,x
         STA     indi1,d
         ADDX    2,i
         LDA     tabInLon,x
         STA     long1,d
         ADDX    2,i
         LDA     tabInLon,x
         STA     indi2,d
         ADDX    2,i
         LDA     tabInLon,x
         STA     long2,d
         STX     xGeneral,d
;
; on compare la longueur de deux valeurs placées consécutivement
; par exemple: la valeur #1 avec la valeur #2
;
         LDA     long1,d  
         CPA     long2,d      ; on compare
         BRGT    inverLo1     ; long1 > long2, pour respecter l'ordre, il faut inverser
         BREQ    mêmeLong     ; long1 = long2, il faut vérifier l'ordre des caractères
         BRLE    pasInvL1     ; long1 < long2, on n'a pas besoin d'inverser      
inverLo1:LDX     xGeneral,d   
         SUBX    6,i          ; reculer pour respecter l'ordre
contOr2: LDA     indi2,d      
         STA     tabInLon,x   ; on inverse l'ordre de l'indice de la valeur #2   
         ADDX    2,i
         LDA     long2,d
         STA     tabInLon,x  ; on inverse l'ordre de la longueur de la valeur #2 
         ADDX    2,i
         LDA     indi1,d
         STA     tabInLon,x  ; on inverse l'ordre de l'indice de la valeur #1
         ADDX    2,i
         LDA     long1,d
         STA     tabInLon,x  ; on inverse l'ordre de la longueur de la valeur #1
         STX     xGeneral,d  ; sauvegarder la position dans le tableau: tabInLon
         BR      bouclOr1
;
; si on a la même longueur, on verifie l'ordre des caractères de valeurs placées consécutivement
; par exemple: la valeur #1 avec la valeur #2 
;
mêmeLong:LDA     indi1,d             
         STA     iTab,d      ; oú commencer la comparaison pour la valeur #1 
         LDA     indi2,d       
         STA     jTab,d      ; oú commencer la comparaison pour la valeur #2
         LDA     long1,d
         STA     cptBCar,d   ; le compteur de boucle de la comparaison des caractères
contComp:LDA     cptBCar,d
         CPA     0,i
         BREQ    pasInvL1    ; pas d'inversement de valeurs 
         SUBA    1,i
         STA     cptBCar,d    
         LDX     iTab,d
         LDBYTEA tab,x       ; récuperer le caractère à comparer «valeur #1» 
         STBYTEA iCar,d        
         ADDX    1,i
         STX     iTab,d      
         LDX     jTab,d
         LDBYTEA tab,x       ; récuperer le caractère à comparer «valeur #2» 
         STBYTEA jCar,d
         ADDX    1,i
         STX     jTab,d
; comparaison des caractères
         CALL    reInit4
         LDBYTEA iCar,d
         CALL    sentChif    ; si le caractère: iCar est un chiffre       
         LDBYTEX jCar,d
         CALL    sentLett    ; si le caractère: jCar est une lettre
         BR      valSupCa    ; validation: les chiffres sont supérieurs aux lettres 
contCom1:STX     carTemp,d
         CPA     carTemp,d
         BREQ    contComp    ; on continue avec la comparaison 
         BRLT    pasInvL1    ; on n'a pas besoin d'inverser
         BRGT    inverLo1    ; pour respecter l'ordre, il faut inverser
pasInvL1:NOP0
         BR      bouclOr1    
finOrLon:RET0
;
; réinitialisation des variables
;
reInit3: LDA     0,i         ; efface le registre A
         STA     nbIterac,d  ; réinitialise le nombre d'itérations
         STA     xGeneral,d  ; réinitialise l'indice dans tab
         STA     indi1,d     ; réinitialise l'indice de la valeur #1
         STA     indi2,d     ; réinitialise l'indice de la valeur #2 
         STA     long1,d     ; réinitialise la longueur de la valeur #1 à comparer
         STA     long2 ,d    ; réinitialise la longueur de la valeur #2 à comparer
         RET0
;
; réinitialisation des variables
;
reInit4: LDA     0,i         ; efface le registre A
         STA     chiffre,d   ; réinitialise la sentinelle de chiffre
         STA     lettre,d    ; réinitialise la sentinelle de lettre
         RET0
;
; affichage de la chaîne de caractères en ordre croissant
;       
affResul:LDX     0,i         
         STX     cptAff,d    
         STRO    enOrdre,d   ; message à afficher 
boucAff0:LDX     cptAff,d    ; le compteur de boucle pour l'affichage
         CPX     cptVal,d    ; on compare le compteur de boucle avec le compteur de valeurs
         BREQ    finAffic    ; oui, on termine     
         ASLX                
         ASLX                
         LDA     tabInLon,x    
         STA     pos,d       
         ADDX    2,i         ; caractère suivant         
         LDA     tabInLon,x    
         STA     longueur,d   
         CHARO   '-',i       ; affiche le symbole -     
         CHARO   '>',i       ; affiche le symbole >     
         CHARO   ' ',i       ; affiche l'espace
         LDX     pos,d       
boucAff1:LDBYTEA tab,x    
         STBYTEA cTemp,d     ; sauvegarde le caractère temporaire pour l'affichage     
         CHARO   cTemp,d     
         ADDX    1,i         
         SUBX    pos,d       
         CPX     longueur,d  ; est-ce que on a fini avec l'affichage de la valeur ?  
         BREQ    prochVal    ; on continue avec la prochaine valeur    
         ADDX    pos,d       
         BR      boucAff1         
prochVal:LDA     cptAff,d  
         ADDA    1,i                
         STA     cptAff,d  
         CHARO   ' ',i    
         BR      boucAff0     
finAffic:CHARO   '\n',i      ; affiche le symbole de saut de ligne
         RET0

;/-----------Variables declaration----------/

paragrap:.ASCII  "Welcome to the program \"SORT\" !"
         .ASCII  "\n\nCe programme sert à trier et afficher en ordre croissant\n"
         .ASCII  "les valeurs codifiées entrées par l'utilisateur\n"
         .ASCII  "\nVous pouvez entrer une ou plusieurs valeurs codifiées ex: -CdE1 -A2\n"
         .ASCII  "suivi d'un «ENTREE» et le programme commencera le traitement \n"
         .ASCII  "\nAttention!\n\n"
	  .ASCII  "Les valeurs à traiter doivent respecter les règles suivantes:\n\n"
         .ASCII  "-être composées du symbole tiret(-) et être séparées par des espaces\n\n"
         .ASCII  "-avoir de 1 à 4 caractères (de a à z) ou (de 0 à 9)\n\n"
         .ASCII  "-le premier caractère doit être obligatoirement une lettre majuscule\n\n"
         .ASCII  "-ne pas dépasser les 5 caractères toute en incluant le tiret\n\n"
         .ASCII  "-les chiffres sont supérieurs aux lettres\n\n"
	  .ASCII  "*La série des données à entrer a un maximum de 40 caractères*\n\n"
         .ASCII  "*Le triage sera en fonction de la longueur de la valeur puis en ordre des caractères*\n\n"
         .BYTE    0
msgerreu:.ASCII  "\nEntrée invalide\n\n"
         .BYTE   0
demande: .ASCII  "\nPlease enter the values to be processed («.» - to finish):"
         .BYTE   0
enOrdre: .ASCII  "\nen ordre: "
         .BYTE   0
normale: .ASCII  "\n\nFin normale du programme."
         .BYTE   0
;***********************************************************************
; les tableaux 
tab:     .BLOCK  39          ; 40 caractères au total
tabInLon:.BLOCK  50          ; tableau des indices et des longueurs 
; les compteurs
cptCar:  .BLOCK  2           ; le compteur de caractères lus #2d, compt1
cptVal:  .BLOCK  2           ; le compteur de valeurs lus #2d, compt2
cptChiff:.BLOCK  2           ; le compteur de chiffres lus dans chaque valeur #2d, compt3
cptLMaj: .BLOCK  2           ; le compteur de lettres majuscules lus dans chaque valeur #2d, compt4
cptLMin: .BLOCK  2           ; le compteur de lettres minuscules lus dans chaque valeur #2d, compt5
cptCaVa: .BLOCK  2           ; le compteur de caractères lus dans chaque valeur #2d, compt6
cptLett: .BLOCK  2           ; le compteur de lettres lus dans chaque valeur #2d, compt7
cptTiret:.BLOCK  2           ; le compteur de tirets lus dans chaque valeur #2d, compt8
cptBOrLo:.BLOCK  2           ; le compteur de boucle pour ordonner par longueur
cptBCar: .BLOCK  2           ; le compteur de boucle pour la comparaison des caractères
cptAff: .BLOCK   2           ; le compteur de boucle pour l'affichage
; les sentinelles 
terminer:.BYTE   0           ; la sentinelle de sortie du programme
tiret:   .BYTE   0           ; la sentinelle de tiret
err:     .BYTE   0           ; la sentinelle d'erreur
derVal:  .BYTE   0           ; la sentinelle de la dernière valeur
chiffre: .BYTE   0           ; la sentinelle de chiffre
lettre:  .BYTE   0           ; la sentinelle de lettre
; autres variables
caract:  .BLOCK  1           ; caractères lus
posTab:  .BLOCK  2           ; position dans le tableau «tab»
posTabIL:.BLOCK  2           ; position dans le tableau «tabInLon»
nbIterac:.BLOCK  2           ; le nombre d'itérations
xGeneral:.BLOCK  2           ; l'indice dans tab
indi1:   .BLOCK  2           ; l'indice de la valeur #1 
indi2:   .BLOCK  2           ; l'indice de la valeur #2 
long1:   .BLOCK  2           ; la longueur de la valeur #1 à comparer
long2:   .BLOCK  2           ; la longueur de la valeur #2 à comparer
iTab:    .BLOCK  2           ; tab [i]
jTab:    .BLOCK  2           ; tab [j]
iCar:    .BLOCK  1           ; le caractère en tab [i]
jCar:    .BLOCK  1           ; le caractère en tab [j]
carCompT:.BLOCK  1           ; le caractère temporaire #1 de comparaison
carTemp: .BLOCK  1           ; le caractère temporaire #2 de comparaison
pos:     .BLOCK  2           ; la position pour l'affichage
longueur:.BLOCK  2           ; la longueur pour l'affichage       
cTemp:   .BLOCK  2           ; le caractère temporaire pour l'affichage
         .END  